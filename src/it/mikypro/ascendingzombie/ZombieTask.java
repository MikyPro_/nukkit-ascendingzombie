package it.mikypro.ascendingzombie;

import cn.nukkit.entity.mob.EntityZombie;
import cn.nukkit.scheduler.PluginTask;

public class ZombieTask extends PluginTask<Miky> {

    private EntityZombie zombie;

    ZombieTask(Miky owner, EntityZombie zombie) {
        super(owner);
        this.zombie = zombie;
    }

    @Override
    public void onRun(int currentTick) {
        zombie.teleport(zombie.getLocation().clone().add(0, 0.05, 0));
    }
}