package it.mikypro.ascendingzombie;

import cn.nukkit.Player;
import cn.nukkit.command.Command;
import cn.nukkit.command.CommandExecutor;
import cn.nukkit.command.CommandSender;
import cn.nukkit.entity.mob.EntityZombie;
import cn.nukkit.nbt.tag.CompoundTag;
import cn.nukkit.nbt.tag.DoubleTag;
import cn.nukkit.nbt.tag.FloatTag;
import cn.nukkit.nbt.tag.ListTag;
import cn.nukkit.plugin.PluginBase;

public class Miky extends PluginBase implements CommandExecutor {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this,this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("startzombie") && args.length == 0) {

            if (sender.isPlayer()) {
                EntityZombie zombie = new EntityZombie(((Player) sender).chunk, new CompoundTag()
                        .putList(new ListTag<DoubleTag>("Pos")
                                .add(new DoubleTag("", ((Player) sender).getPosition().getX()))
                                .add(new DoubleTag("", ((Player) sender).getPosition().getY()))
                                .add(new DoubleTag("", ((Player) sender).getPosition().getZ())))
                        .putList(new ListTag<DoubleTag>("Motion")
                                .add(new DoubleTag("", 0))
                                .add(new DoubleTag("", 0))
                                .add(new DoubleTag("", 0)))
                        .putList(new ListTag<FloatTag>("Rotation")
                                .add(new FloatTag("", 0))
                                .add(new FloatTag("", 0))));
                zombie.spawnTo(((Player) sender));
                getServer().getScheduler().scheduleRepeatingTask(new ZombieTask(this, zombie), 1);
                return true;
            }
            sender.sendMessage("This command is only for players use.");
            return true;
        }
        return false;
    }
}
